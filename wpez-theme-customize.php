<?php
/**
 * Bootstraps the plugin
 *
 * @package WPezThemeCustomizeSukiForAU
 */

/*
Plugin Name: WPezPlugins: Theme Customize: Suki for Alchemy United
Plugin URI: https://gitlab.com/WPezPlugins/wpez-theme-customize
Description: Boilerplate for customizations of your theme / parent theme.
Version: 0.0.5
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: suki_for_au
 */

namespace WPezThemeCustomizeSukiForAU;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

use WPezThemeCustomizeSukiForAU\App\ClassPlugin;

$str_php_ver_comp = '5.4.0';

// we reserve the right to use traits :).
if ( version_compare( PHP_VERSION, $str_php_ver_comp, '<' ) ) {
	exit( sprintf( 'WPezPlugins: Theme Customize requires PHP ' . esc_html( $str_php_ver_comp ) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION ) );
}

/**
 * Gets the autoloader sorted out.
 *
 * @param boolean $bool Toggle on or off.
 * @return void
 */
function autoloader( $bool = true ) {

	if ( true !== $bool ) {
		return;
	}

	require_once 'App/Core/Autoload/ClassAutoload.php';

	$new_autoload = new ClassAutoload();

	$new_autoload->setPathBase( plugin_dir_path( __FILE__ ) );
	$new_autoload->setNeedleRoot( __NAMESPACE__ );
	$new_autoload->setNeedleChild( 'App' );

	spl_autoload_register( array( $new_autoload, 'wpezAutoload' ), true );
}
autoloader();

/**
 * Undocumented function
 *
 * @param boolean $bool Toggle on or off.
 * @return void
 */
function plugin( $bool = true ) {

	if ( true !== $bool ) {
		return;
	}

	include 'App/Theme/functions-plugged.php';

	$new_plugin = new ClassPlugin();
}
plugin();
