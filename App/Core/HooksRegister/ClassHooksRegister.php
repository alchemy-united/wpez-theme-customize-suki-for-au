<?php
/**
 * As inspired by:
 *
 * https://code.tutsplus.com/tutorials/object-oriented-programming-in-wordpress-building-the-plugin-ii--cms-21105
 *
 * https://github.com/DevinVinson/WordPress-Plugin-Boilerplate/blob/master/plugin-name/includes/class-plugin-name-loader.php
 *
 * The notable ez improvement is loading is done with arrays, not (harcoded)
 * code. This means, you can define your hooks and then - prior to loading -
 * slap a filter on those arrays to allow for customization by users (read:
 * other devs). As a result any hook'ed callback (method) can be replaced as
 * needed.
 *
 * Yeah. Cool :)
 *
 */

namespace WPezThemeCustomizeSukiForAU\App\Core\HooksRegister;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassHooksRegister {

	/**
	 * Actions to be added
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Filters to be added
	 *
	 * @var array
	 */
	protected $arr_filters;

	/**
	 * Array of details for a given action / filter that's to be added
	 *
	 * @var array
	 */
	protected $arr_hook_defaults;


	/**
	 * Construct it.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Initializes any properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_actions = [];
		$this->arr_filters = [];

		$this->arr_hook_defaults = [
			'active'        => true,
			'hook'          => false,
			'component'     => false,
			'callback'      => false,
			'priority'      => 10,
			'accepted_args' => 1,
		];
	}


	/**
	 * Push a single hook on to the array of actions.
	 *
	 * @param array $arr_args
	 * @return bool
	 */
	public function pushAction( $arr_args = false ) {

		return $this->pushMaster( 'arr_actions', $arr_args );
	}

	/**
	 *  Push a single hook on to the array of filters.
	 *
	 * @param boolean $arr_args
	 * @return void
	 */
	public function pushFilter( $arr_args = false ) {

		return $this->pushMaster( 'arr_filters', $arr_args );
	}


	/**
	 * Does the push for pushAction and pushFilter.
	 *
	 * @param boolean $str_prop
	 * @param boolean $arr_args
	 * @return void
	 */
	protected function pushMaster( $str_prop = false, $arr_args = false ) {

		if ( ! is_array( $arr_args ) ) {
			return false;
		}

		$obj_hook = (object) array_merge( $this->arr_hook_defaults, $arr_args );

		// maybe we have a problem?
		if ( true !== $obj_hook->active || false === $obj_hook->hook || false === $obj_hook->component || false === $obj_hook->callback ) {
			return false;
		}

		$this->$str_prop[] = $obj_hook;
		return true;
	}

	/**
	 * Load an array of action hooks. 
	 *
	 * @param array $arr_arrs Array of action arrays
	 * @return void
	 */
	public function loadActions( $arr_arrs = false ) {

		if ( ! is_array( $arr_arrs ) ) {
			return false;
		}

		$this->loadMaster( 'pushAction', $arr_arrs );
		return true;
	}

	/**
	 * Load an array of filters hooks. 
	 *
	 * @param array $arr_arrs Array of filter arrays
	 * @return bool
	 */
	public function loadFilters( $arr_arrs = false ) {

		if ( ! is_array( $arr_arrs ) ) {
			return false;
		}
		$this->loadMaster( 'pushFilter', $arr_arrs );
		return true;
	}


	/**
	 * Loops over the load* $arr_arrs and uses the appropriate push method.
	 *
	 * @param string $str_method
	 * @param array $arr_arrs
	 * @return void
	 */
	protected function loadMaster( $str_method, $arr_arrs = [] ) {

		foreach ( $arr_arrs as $arr){

			$this->$str_method( $arr );
		}
	}


	/**
	 * After pushing or loading, then register the hooks.
	 *
	 * @param boolean $bool_actions
	 * @param boolean $bool_filters
	 * @return void
	 */
	public function doRegister( $bool_actions = true, $bool_filters = true ) {

		if ( true == $bool_actions && ! empty( $this->arr_actions ) ) {

			$this->register( 'add_action', $this->arr_actions );
		}

		if ( true === $bool_filters && ! empty( $this->arr_filters ) ) {

			$this->register( 'add_filter', $this->arr_filters );
		}
	}

	/**
	 * Does the hook register'ing for doRegister() method.
	 *
	 * @param string $str_wp_function
	 * @param array $arr_objs
	 * @return void
	 */
	protected function register( $str_wp_function = '', $arr_objs = [] ) {

		foreach ( $arr_objs as $obj ) {

			$str_wp_function( $obj->hook, [ $obj->component, $obj->callback ], $obj->priority, $obj->accepted_args );
		}
	}

}
