<?php
/*
Plugin Name: WPezPlugins: Theme Customize: TODO
Plugin URI: https://gitlab.com/WPezPlugins/wpez-theme-customize
Description: Boilerplate for customizations of your theme / parent theme.
Version: 0.0.2
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: TODO
*/

namespace WPezThemeCustomizeSukiForAU\App;

use WPezThemeCustomizeSukiForAU\App\Core\HooksRegister\ClassHooksRegister;

use WPezThemeCustomizeSukiForAU\App\Theme\Actions\ClassActions;
use WPezThemeCustomizeSukiForAU\App\Theme\Filters\ClassFilters;
use WPezThemeCustomizeSukiForAU\App\Theme\Other\ClassOther;
use WPezThemeCustomizeSukiForAU\App\Theme\WPCore\ClassWPCore;

/**
 * Undocumented class
 */
class ClassPlugin {

	/**
	 * Array of action hooks
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Array of filter hooks
	 *
	 * @var array
	 */
	protected $arr_filters;

	/**
	 * Undocumented variable
	 *
	 * @var object
	 */
	protected $new_wpcore;


	/**
	 * The __construct().
	 */
	public function __construct() {

		$this->set_property_defaults();

		$this->actions( true );

		$this->filters( true );

		$this->other( false );

		$this->wpCore( true );

		// this should be last.
		$this->hooksRegister();

	}

	protected function set_property_defaults() {

		$this->arr_actions   = array();
		$this->arr_filters   = array();

		$this->new_wpcore = new ClassWPCore();
	}

	/**
	 * After gathering (below) the arr_actions and arr_filter, it's time to
	 * make some RegisterHook magic
	 */
	protected function hooksRegister() {

		$new_hooks_reg = new ClassHooksRegister();

		$new_hooks_reg->loadActions( $this->arr_actions );

		$new_hooks_reg->loadFilters( $this->arr_filters );

		$new_hooks_reg->doRegister();
	}


	public function actions( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$new_actions = new ClassActions();

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'init',
			'component' => $new_actions,
			'callback'  => 'addPostTypeSupport',  // ok
			'priority'  => 100,
		];

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/header/mobile-vertical/after-element',
			'component' => $new_actions,
			'callback'  => 'mobileVerticalAfterElement', // ok
		];

		$this->arr_actions[] = [
			'active'    => true, 
			'hook'      => 'suki/frontend/header',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiMainHeader', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => false, // <<< false
			'hook'      => 'suki/frontend/logo',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiFrontendLogo',  // ok?
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => true, 
			'hook'      => 'suki/frontend/after_main',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiFrontendAfterMain', // ok
			'priority'  => 5,
		];

		// Removes the comment title.
		$this->arr_actions[] = [
			'active'    => true, 
			'hook'      => 'suki/frontend/before_comments_list',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiFrontendBeforeCommentsList',
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/frontend/mobile_logo',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiFrontendMobileLogo', // ok 
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/frontend/mobile_logo',
			'component' => $new_actions,
			'callback'  => 'customSukiDefaultMobileLogo', // ok
			'priority'  => 15,
		];

		$this->arr_actions[] = [
			'active'    => true,
		//	'hook'      => 'suki/frontend/after_header', suki/frontend/entry/header_custom
			'hook'      => 'suki/frontend/entry/header_custom',
		//	'hook'      => 'suki/frontend/page_content/before_header',			
			'component' => $new_actions,
			'callback'  => 'sukiFrontendAfterHeader', // ok
			'priority'  => 15,
		];

		$this->arr_actions[] = [
			'active'    => false,  // <<< false 
			'hook'      => 'suki/frontend/entry_grid/header',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiFrontendEntryGridHeader', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => false, // <<< false
			'hook'      => 'suki/frontend/entry/before_header',
			'component' => $new_actions,
			'callback'  => 'customSukiFrontendEntryBeforeHeaderWrapperOpen', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => false, // <<< false
			'hook'      => 'suki/frontend/entry/after_header',
			'component' => $new_actions,
			'callback'  => 'customSukiFrontendEntryBeforeHeaderWrapperClose', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => true,  // <<, false - remove, this is where the JS used to be 
			'hook'      => 'au_suki/frontend/before_main',
			'component' => $new_actions,
			'callback'  => 'customSukiFrontendBeforeAddHeaderToBlog', // ok
			'priority'  => 5,
		];

	//	add_action( 'suki/frontend/archive_header', 'suki_archive_title', 10 );

			$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/frontend/archive_header',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiFrontendArchiveHeader', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => true,  // <<, false - remove, this is where the JS used to be 
			'hook'      => 'suki/frontend/archive_header',
			'component' => $new_actions,
			'callback'  => 'customSukiFrontendBeforeAddHeaderToBlog', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/frontend/after_main',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiSinglePostNavigation', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => false, // <<< false
			'hook'      => 'suki/frontend/after_main',
			'component' => $new_actions,
			'callback'  => 'sukiSinglePostNavigation', // ok
			'priority' => 15,
		];

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/frontend/footer',
			'component' => $new_actions,
			'callback'  => 'sukiFrontendBeforeFooter', // ok
			'priority'  => 5,
		];

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/frontend/footer',
			'component' => $new_actions,
			'callback'  => 'sukiFrontendBeforeFooterBrand', // ok
			'priority'  => 7,
		];

		$this->arr_actions[] = [
			'active'    => true,
			'hook'      => 'suki/frontend/entry/before_footer',
			'component' => $new_actions,
			'callback'  => 'removeActionSukiFrontendEntryBeforeFooterEntryTags', // ok
			'priority' => 5,
		];

	}

	public function filters( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$new_filters = new ClassFilters();

		$this->arr_filters[] = [
			'active'        => false, // <<< false
			'hook'          => 'body_class',
			'component'     => $new_filters,
			'callback'      => 'bodyClass',
			// 'priority' => 75,
			'accepted_args' => 1,
		];

		$this->arr_filters[] = [
			'hook'          => 'suki/frontend/page_header_element/title',
			'component'     => $new_filters,
			'callback'      => 'remove_title_404',  // ok
			//	'priority'  => 75,
			'accepted_args' => 1,
		];

		$this->arr_filters[] = [
			'hook'          => 'suki/frontend/content_classes',
			'component'     => $new_filters,
			'callback'      => 'sukiFrontendContentClasses', // ok
			'priority'      => 75,
			'accepted_args' => 1,
		];

		$this->arr_filters[] = [
			'hook'          => 'get_search_form',
			'component'     => $new_filters,
			'callback'      => 'getSearchForm', // ok
			// 'priority' => 75,
			'accepted_args' => 2,
		];

		$this->arr_filters[] = [
			'hook'      => 'after_setup_theme',
			'component' => $new_filters,
			'callback'  => 'removeSukiGetSearchFormAddIcon', // ok 
			// 'priority' => 75
		];

		$this->arr_filters[] = [
			'active'	=> false, // <<< false
			'hook'      => 'suki/frontend/entry/thumbnail_classes',
			'component' => $new_filters,
			'callback'  => 'frontendEntryThumbnailClasses', // ok
			// 'priority' => 75
		];

		$this->arr_filters[] = [
			'hook'      => 'suki/dataset/social_media_types',
			'component' => $new_filters,
			'callback'  => 'sukiDatasetSocialMediaTypes', // ok
			// 'priority' => 75
		];

		$this->arr_filters[] = [
			'hook'      => 'suki/frontend/svg_icon',
			'component' => $new_filters,
			'callback'  => 'sukiFrontendSvgIcon', // ok
			'priority' => 15,
			'accepted_args' => 2
		];

		$this->arr_filters[] = [
			'hook'      => 'suki/frontend/comment_form_args',
			'component' => $new_filters,
			'callback'  => 'frontendCommentFormArgs', // ok
			// 'priority' => 75
		];

		$this->arr_filters[] = [
			'active'        => false, // <<< false
			'hook'          => 'wp_calculate_image_sizes',
			'component'     => $new_filters,
			'callback'      => 'wp_calculate_image_sizes',// ok
			'priority' 		=> 75,
			'accepted_args' => 5,
		];


	}


	public function other( $bool = true ) {

		if ( $bool !== true ) {
			return;
		}

		$new_other = new ClassOther();
	}


	public function wpCore( $bool = true ) {

		if ( $bool !== true ) {
			return;
		}

		$this->wpDequeueStyle( false );

		$this->wpDequeueScript( false );

		$this->deregisterPostType( false );

		$this->postTypeSupport( false );

		$this->unregisterSidebar( true );

		$this->unregisterWidget( false );

		$this->removeSetting( false );
	}


	protected function wpDequeueStyle( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$arr_args = [
			// 'style-name-1' => true, // true = remove
			// 'style-name-2' => false // false = leave
		];
		$this->new_wpcore->setStyles( $arr_args );

		$this->arr_actions[] = [
			'hook'      => 'wp_enqueue_scripts',
			'component' => $this->new_wpcore,
			'callback'  => 'wpDequeueStyle',
			'priority'  => 100,
		];
	}


	protected function wpDequeueScript( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$arr_args = [
			// 'script-name-1' => true,
			// 'script-name-2' => false
		];
		$this->new_wpcore->setScripts( $arr_args );

		$this->arr_actions[] = [
			'hook'      => 'wp_enqueue_scripts',
			'component' => $this->new_wpcore,
			'callback'  => 'wpDequeueScript',
			'priority'  => 100,
		];
	}


	protected function deregisterPostType( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$arr_args = [
			// 'post-type-1' => true,
			// 'post-type-2' => false
		];
		$this->new_wpcore->setPostTypes( $arr_args );

		$this->arr_actions[] = [
			'hook'      => 'init',
			'component' => $this->new_wpcore,
			'callback'  => 'deregisterPostType',
			'priority'  => 100,
		];
	}


	protected function postTypeSupport( $bool = true ) {
		
		if ( true !== $bool ) {
			return;
		}

		$arr_args = [
			// 'support-1' => true,
			// 'support-2' => false,
		];
		$this->new_wpcore->setPostTypesSupport( $arr_args );

		$this->arr_actions[] = [
			'hook'      => 'init',
			'component' => $this->new_wpcore,
			'callback'  => 'removePostTypeSupport',
			'priority'  => 100,
		];

		$this->arr_actions[] = [
			'hook'      => 'init',
			'component' => $this->new_wpcore,
			'callback'  => 'addPostTypeSupport',
			'priority'  => 105,
		];
	}


	protected function unregisterSidebar( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$arr_args = [
			'footer-widgets-5' => true,
			'footer-widgets-6' => true,
		];
		$this->new_wpcore->setSidebars( $arr_args );

		$this->arr_actions[] = [
			'hook'      => 'init',
			'component' => $this->new_wpcore,
			'callback'  => 'unregisterSidebar',
			'priority'  => 100,
		];

	}


	protected function unregisterWidget( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$arr_args = [
			// 'widget-name-1' => true,
			// 'widget-name-2' => false
		];
		$this->new_wpcore->setWidgets( $arr_args );

		$this->arr_actions[] = [
			'hook'      => 'init',
			'component' => $this->new_wpcore,
			'callback'  => 'unregisterWidget',
			'priority'  => 100,
		];

	}

	protected function removeSetting( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		// Don't forget to use set_theme_mod() to "replace" the values for the settings you remove.
		$arr_args = [
			// 'setting-name-1' => true,
			// 'setting-name-2' => false
		];
		$this->new_wpcore->setSettings( $arr_args );

		$this->arr_actions[] = [
			'hook'      => 'customize_register',
			'component' => $this->new_wpcore,
			'callback'  => 'removeSetting',
			'priority'  => 100,
		];

	}
}
