<?php

namespace WPezThemeCustomizeSukiForAU\App\Theme\WPCore;

class ClassWPCore {

    protected $_styles;
    protected $_scripts;
    protected $_post_types;
    protected $_post_types_support;
    protected $_sidebars;
    protected $_widgets;
    protected $_settings;

    // protected $_properties = [ 'scripts', 'styles', 'post_types', 'post_types_support', 'sidebars', 'widgets', 'settings' ];

    public function __construct() {

        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults() {

        $this->_styles             = [];
        $this->_scripts            = [];
        $this->_post_types         = [];
        $this->_post_types_support = [];
        $this->_sidebars           = [];
        $this->_widgets            = [];
        $this->_settings           = [];
    }


    /**
     * public function set( $property, $value ) {
     *
     * if ( in_array( $property, $this->_properties ) && is_array( $value ) ) {
     * $property        = '_' . trim($property);
     * $this->$property = $value;
     * }
     * }
     */

    /**
     * @param $arr
     * @param $arr_default
     *
     * @return mixed
     */
    protected function setArray( $arr, $arr_default ) {

        if ( is_array( $arr ) ) {
            return $arr;
        }

        return $arr_default;
    }


    public function setStyles( $arr = false ) {

        $this->_styles = $this->setArray( $arr, $this->_styles );
    }

    public function setScripts( $arr = false ) {

        $this->_scripts = $this->setArray( $arr, $this->_scripts );
    }

    public function setPostTypes( $arr = false ) {

        $this->_post_types = $this->setArray( $arr, $this->_post_types );
    }

    public function setPostTypesSupport( $arr = false ) {

        $this->_post_types_support = $this->setArray( $arr, $this->_post_types_support );
    }

    public function setSidebars( $arr = false ) {

        $this->_sidebars = $this->setArray( $arr, $this->_sidebars );
    }

    public function setWidgets( $arr = false ) {

        $this->_widgets = $this->setArray( $arr, $this->_widgets );
    }

    public function setSettings( $arr = false ) {

        $this->_settings = $this->setArray( $arr, $this->_settings );
    }


    /**
     * @param $str_function
     * @param $arr_args
     *
     * @return bool
     */
    protected function unWP( $str_function, $arr_args ) {

        if ( is_array( $arr_args ) ) {

            foreach ( $arr_args as $arg => $bool ) {
                if ( ! isset( $bool ) || $bool !== false ) {
                    $str_function( $arg );
                }
            }

            return true;
        }

        return false;
    }


    public function wpDequeueStyle() {

        return $this->unWP( 'wp_dequeue_style', $this->_styles );
    }


    public function wpDeregisterStyle() {

        return $this->unWP( 'wp_deregister_style', $this->_styles );
    }


    public function wpDequeueScript() {

        return $this->unWP( 'wp_dequeue_script', $this->_scripts );
    }


    public function wpDeregisterScript() {

        return $this->unWP( 'wp_deregister_script', $this->_scripts );
    }


    /**
     * not in core per se but close enough ;)
     */
    public function deregisterPostType() {

        if ( is_array( $this->_post_types ) ) {
            global $wp_post_types;
            foreach ( $this->_post_types as $post_type => $bool ) {
                if ( isset( $wp_post_types[ $post_type ] ) && ( ! isset( $bool ) || $bool !== false ) ) {
                    unset( $wp_post_types[ $post_type ] );
                }
            }
        }
    }


    protected function postTypeSupport( $str_function, $str_support_type ) {

        if ( is_array( $this->_post_types_support ) ) {

            global $wp_post_types;
            foreach ( $this->_post_types_support as $post_type => $support_types ) {

                if ( isset( $wp_post_types[ $post_type ] ) && isset( $support_types[ $str_support_type ] ) && is_array( $support_types[ $support_type ] ) ) {

                    foreach ( $support_types[ $str_support_type ] as $support => $bool ) {

                        if ( ! isset( $bool ) || $bool !== false ) {
                            $str_function( $post_type, $support );
                        }
                    }
                }
            }

            return true;
        }

        return false;
    }


    public function removePostTypeSupport() {

        return $this->postTypeSupport( 'remove_post_type_support', 'remove' );
    }


    public function addPostTypeSupport() {

        return $this->postTypeSupport( 'add_post_type_support', 'add' );
    }


    public function unregisterSidebar() {

        return $this->unWP( 'unregister_sidebar', $this->_sidebars );
    }


    public function unregisterWidget() {

        return $this->unWP( 'unregister_widget', $this->_widgets );
    }


    /**
     * not in core per se but close enough ;)
     */
    public function removeSetting($wp_customize) {

        if ( is_array( $this->_settings ) ) {
            foreach ( $this->_settings as $setting => $bool ) {
                if ( is_string($setting) && ( ! isset( $bool ) || $bool !== false ) ) {
                    $wp_customize->add_control( $setting, []);
                }
            }
        }
    }

}
