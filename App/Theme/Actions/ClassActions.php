<?php

namespace WPezThemeCustomizeSukiForAU\App\Theme\Actions;

class ClassActions {

	public function __construct(){}


	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function addPostTypeSupport() {

		// TODO - build new class for PostTypeSupport and move this to functions-php-for-*
		add_post_type_support( 'page', 'excerpt' );

	}

	/**
	 * TODO - ok
	 *
	 * @param [type] $element
	 * @return void
	 */
	public function mobileVerticalAfterElement( $element ) {

		if ( 'mobile-menu' === $element ) {

			echo '<div class="au-menu-search-wrap">';
				get_search_form( array( 'echo' => true ) );
			echo '</div>';
			
			$this->brandPackage();
		}
	}

	/**
	 * TODO -ok
	 *
	 * @return void
	 */
	public function sukiFrontendBeforeFooterBrand() {
		$this->brandPackage();
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function brandPackage() {
		?>
		<div class="menu-brand-wrapper">
			<div>
				<p><?php echo esc_attr( get_bloginfo( 'name' ) ); ?></p>
				<p class="blog-info-description"><?php echo esc_attr( get_bloginfo( 'description' ) ); ?></p>
			</div>
		</div>
		<?php
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function removeActionSukiMainHeader() {

		remove_action( 'suki/frontend/header', 'suki_main_header', 10 );
	}

	/**
	 * TODO - ok - active currently false
	 *
	 * @return void
	 */
	public function removeActionSukiFrontendLogo() {
	
		remove_action( 'suki/frontend/logo', 'suki_default_logo', 10 );
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function removeActionSukiFrontendArchiveHeader() {

		// add_action( 'suki/frontend/archive_header', 'suki_archive_title', 10 );
		// add_action( 'suki/frontend/archive_header', 'suki_archive_description', 20 );

		remove_action( 'suki/frontend/archive_header', 'suki_archive_title', 10 );
		remove_action( 'suki/frontend/archive_header', 'suki_archive_description', 20 );

	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function removeActionSukiFrontendAfterMain() {

		remove_action( 'suki/frontend/after_main', 'suki_archive_navigation', 10 );

		remove_action( 'suki/frontend/after_main', 'suki_post_author_bio', 10 );
		remove_action( 'suki/frontend/after_main', 'suki_post_navigation', 15 );
		remove_action( 'suki/frontend/after_main', 'suki_comments', 20 );
	}

	/**
	 * Removes the comment area title.
	 *
	 * @return void
	 */
	public function removeActionSukiFrontendBeforeCommentsList() {

		remove_action( 'suki/frontend/before_comments_list', 'suki_comments_title', 10 );

	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function removeActionSukiFrontendMobileLogo() {
		remove_action( 'suki/frontend/mobile_logo', 'suki_default_mobile_logo', 10 );
	}



	/**
	 * TODO - ok 
	 *
	 * @return void
	 */
	public function customSukiDefaultMobileLogo() {
		$this->svgLogoMobile();
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	protected function svgLogoMobile() {
		$this->svgLogo();
	}


	/**
	 * TODO - ok
	 *
	 * @param [type] $str
	 * @param [type] $ndx
	 * @return void
	 */
	public function arrayMapName( $str, $ndx ) {

		echo '<span class="logo-name-word-' . esc_attr( $ndx ) . '">' . esc_attr( $str ) . '</span>';
	}


	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	protected function svgLogo() {

		echo '<div class="sr-only">' , esc_html( get_bloginfo( 'name' ) ) , '</div>';
		echo '<span class="svg-logo-wrap">';
		$arr_name         = explode( ' ', get_bloginfo( 'name' ) );
		$arr_name_escaped = array_map( array( $this, 'arrayMapName' ), $arr_name, array_keys( $arr_name ) );

		// echo implode( ' ', $arr_name_escaped );
		echo '</span>';
	}

	/**
	 * TODO - ok - active currently false
	 *
	 * @return void
	 */
	public function removeActionSukiFrontendEntryGridHeader() {
		if ( is_home() ) {
			remove_action( 'suki/frontend/entry_grid/header', 'suki_entry_grid_header_meta', 10 );
		}
	}

	/**
	 * TODO - ok - active currently set to false
	 *
	 * @return void
	 */
	public function customSukiFrontendEntryBeforeHeaderWrapperOpen() {

		// echo '<div class="hero-wrapper">';
	}

	/**
	 * TODO - ok - active currently set to false
	 *
	 * @return void
	 */
	public function customSukiFrontendEntryBeforeHeaderWrapperClose() {

		// echo '</div>';
	}


	public function customSukiFrontendBeforeAddHeaderToBlog() {

		?>
		<header class="page-header">

			<?php
			if ( is_home() && 'au_suki/frontend/before_main' === current_action() ) {

				echo '<h1 class="page-title">';
				echo esc_attr( __( 'Blog ', 'suki_for_au ' ) );
				echo '</h1>';

			} elseif ( is_archive() ) {

				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );

			} elseif ( is_search() ) {

				//	do_action( 'suki/frontend/before_main' );
				echo '<div class="details-inner details-search">';
				echo '<h1 class="page-title">Search Results:</h1>';
				get_search_form( array( 'echo' => true ) );
				echo '</div>';

			}

			global $wp_query;
			$max_num_pages = (int) $wp_query->max_num_pages;
			$paged         = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			echo '<span class="posts-is-paged">';
			echo esc_attr( __( 'Page: ', 'suki_for_au' ) ) . \esc_attr( $paged ) . esc_attr( __( ' of ', 'suki_for_au' ) ) . \esc_attr( $max_num_pages );
			echo '</span>';
			?>
		</header>
		<?php
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function sukiFrontendAfterHeader() {

		if ( is_single() || is_page() ) {

			/**
			 * Hook: suki/frontend/entry/before_header
			 *
			 * @hooked suki_entry_featured_media - 10
			 */
			do_action( 'suki/frontend/entry/before_header' );

			if ( has_action( 'suki/frontend/entry/header' ) ) {
				?>
				<div id="wpez-h2c" class="hero-wrapper wpez-qs-hero">
				<header id="hero-wrapper" class="entry-header <?php echo esc_attr( 'suki-text-align-' . suki_get_theme_mod( 'entry_header_alignment' ) ); ?>">
					<?php
					/**
					 * Hook: suki/frontend/entry/header
					 *
					 * @hooked suki_entry_header_meta - 10
					 * @hooked suki_entry_title - 20
					 */
					do_action( 'suki/frontend/entry/header' );

					$this->sukiFrontendEntryHeaderExcerpt();
					?>
				</header>
				<div class="au-tagline"><?php echo esc_attr( get_bloginfo( 'description' ) ); ?></div>
				</div>
				<?php
			}
		}
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function sukiFrontendEntryHeaderExcerpt() {

		if ( is_single() || is_page() ) {

			global $post;

			if ( ! empty( $post->post_excerpt ) ){

				echo '<h2 class="entry-excerpt">';
				echo wp_kses( $post->post_excerpt, 'post' );
				echo '</h2>';
			}
		}
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function removeActionSukiFrontendEntryBeforeFooterEntryTags() {

		if ( is_single() ) {

			remove_action( 'suki/frontend/entry/before_footer', 'suki_entry_tags', 10 );
		}
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function removeActionSukiSinglePostNavigation() {

		remove_action( 'suki/frontend/after_main', 'suki_single_post_navigation', 15 );
	}


	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function sukiFrontendBeforeFooter () {

		echo '<div class="' . esc_attr( 'suki-content-layout-' . suki_get_current_page_setting( 'content_layout' ) ) . '">';
		echo '<div class="suki-wrapper">';
		echo '<div class="entry-content choice-of-action">';
		echo '<h3>' . esc_attr( 'Next action?' ) . '</h3>';

		$this->newsletterSignup();

		$this->sukiSocial();

		if ( is_single() && 'post' === get_post_type() && ( comments_open() || get_comments_number() ) ) {
			$this->singleComments();
		}

		$this->singleNextPrevPosts();

		$this->blogCategories();

		$this->blogTagsTop();

		$this->blogPostsTrending();

		$open = false;
		if ( is_paged() ) {
			$open = true;
		}
		$this->blogPaging( $open );

		$open = false;
		if ( is_404() || is_search() ) {
			$open = true;
		}
		$this->searchForm( $open );
	
		$this->searchTopTerms();

		echo '<div class="au-back-to-top">';
		echo '<a id="au-b2t" class="au-b2t" href="#" title="Back to Top"><span class="triangle-up"></span></a>';
		echo '<br>';
		echo '<a id="au-b2t-text" class="au-b2t" href="#" title="Back to Top">Back to Top</a>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		echo '</div>';

	}


	protected function newsletterSignup( bool $active = true, bool $echo = true ) {

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}

		$scode = '[wpez_hsfa]';
		$nl_form = do_shortcode( $scode );

		if ( empty( $nl_form ) || $scode === $nl_form ) {
			return;
		}

		$summary = 'Newsletter Signup';

		$ret .= '<details id="au-newsletter-signup">';
		$ret .= '<summary>' . esc_html( $summary) . '</summary>';
		$ret .= '<div class="details-inner details-newsletter">';

		$arr_kses_allow = array(
			'button'   => array(
				'disabled' => true,
				'type'     => true,
			),
			'div'      => array(
				'class' => true,
			),
			'form'     => array(
				'class'  => true,
				'id'     => true,
				'method' => true,
			),
			'fieldset' => array(
				'class' => true,
			),
			'input'    => array(
				'class'       => true,
				'data-hidden' => true,
				'id'          => true,
				'maxlength'   => true,
				'name'        => true,
				'size'        => true,
				'type'        => true,
				'required'    => true,
				'value'       => true,
			),		
			'label'    => array(
				'class' => true,
				'for'   => true,
			),
			'legend'   => array(
				'span' => true,
				'div'  => true,
			),
			'p'        => array(
				'class' => true,
			),
		);

		$ret .= wp_kses( $nl_form, $arr_kses_allow);

		// $ret .= $nl_form;
		$ret .= '</div>';
		$ret .= '</details>';

		return $this->maybeEcho( $ret, $echo );
	}

	protected function singleComments( bool $active = true, bool $echo = true ) {

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}

		$summary = 'Blog: Comments';

		\ob_start();
		comments_template();
		$comments = \ob_get_contents();
		\ob_end_clean();

		$ret .= '<details id="details-comments">';
		$ret .= '<summary>' .  wp_kses_post( $summary ) . '</summary>';
		$ret .= '<div class="details-inner details-comments">';
		// $ret .= '<p>';
		$ret .= $comments;
		// $ret .= '</p>';
		$ret .= '</div>';
		$ret .= '</details>';

		return $this->maybeEcho( $ret, $echo );
	}

	protected function blogPaging( bool $open = false, bool $active = true, bool $echo = true ){

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}

		$get_loop_nav = $this->loop_navigation();
		if ( ! empty( $get_loop_nav ) ) {

			if ( $open ) {
				$ret .= '<details open>';
			} else {
				$ret .= '<details open>';
			}

			$ret .= '<summary>' . 'Blog /  Search: Paging' . '</summary>';
			$ret .= '<div class="details-inner details-blog-paging">';
			$ret .= wp_kses_post( $get_loop_nav );
			$ret .= '</div>';
			$ret .= '</details>';
		}
		return $this->maybeEcho( $ret, $echo );
	}


	/**
	 * Render posts loop navigation.
	 */
	function loop_navigation() {

		if ( ! is_archive() && ! is_home() && ! is_search() ) {
			return;
		}

		// Render posts navigation.
		switch ( suki_get_theme_mod( 'blog_index_navigation_mode' ) ) {
			case 'pagination':
				return get_the_posts_pagination( 
					array(
						'mid_size'  => 2,
						'prev_text' => '&laquo; Prev',
						'next_text' => 'Next &raquo;',
						'prev_text' => '<',
						'next_text' => '>',
					)
				);
				break;

			default:
				return get_the_posts_navigation(
					array(
						'prev_text' => esc_html__( 'Older Posts', 'suki' ) . ' &raquo;',
						'next_text' => '&laquo; ' . esc_html__( 'Newer Posts', 'suki' ),
					) 
				);
				break;
		}
	}

	protected function blogCategories( bool $active = true, bool $echo = true ) {

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}

		if ( has_nav_menu( 'menu_blog_categories' ) ) {

			$ret .= '<details>';
			$ret .= '<summary>' . 'Blog: Categories' . '</summary>';
			$ret .= '<div class="details-inner details-nav details-blog-catagories">';
			$ret .= '<nav class="footer-nav footer-categories-nav">';
			$ret .= wp_nav_menu(
				array(
					'theme_location' => 'menu_blog_categories',
					'menu_class'     => 'menu',
					'container'      => true,
					'echo'           => false,
					'depth'          => -1,
				)
			);
			$ret .= '</nav>';
			$ret .= '</div>';
			$ret .= '</details>';
		}

		return $this->maybeEcho( $ret, $echo );
	}

	protected function blogTagsTop( bool $active = true, bool $echo = true ){

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}

		if ( has_nav_menu( 'menu_blog_tags_tops' ) ) {

			$ret .= '<details>';
			$ret .= '<summary>' . 'Blog: Top Tags' . '</summary>';
			$ret .= '<div class="details-inner details-nav details-blog-tags">';
			$ret .= '<nav class="footer-nav footer-tags-nav">';
			$ret .= wp_nav_menu(
				array(
					'theme_location' => 'menu_blog_tags_tops',
					'menu_class'     => 'menu',
					'container'      => true,
					'echo'           => false,
					'depth'          => -1,
				)
			);
			$ret .= '</nav>';
			$ret .= '</div>';
			$ret .= '</details>';
		}

		return $this->maybeEcho( $ret, $echo );
	}

	protected function blogPostsTrending( bool $active = true, bool $echo = true ) {

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}

		if ( has_nav_menu( 'menu_blog_posts_trending' ) ) {

			$ret .= '<details>';
			$ret .= '<summary>' . 'Blog: Trending Articles' . '</summary>';
			$ret .= '<div class="details-inner details-nav details-blog-trending">';
			$ret .= '<nav class="footer-nav footer-trending-nav">';
			$ret .= wp_nav_menu(
				array(
					'theme_location' => 'menu_blog_posts_trending',
					'menu_class'     => 'menu',
					'container'      => true,
					'echo'           => false,
					'depth'          => -1,
				)
			);
			$ret .= '</nav>';
			$ret .= '</div>';
			$ret .= '</details>';
		}

		return $this->maybeEcho( $ret, $echo );
	}

	protected function singleNextPrevPosts( bool $active = true, bool $echo = true ){

		$ret = '';
		if ( true !== $active || ! is_single() ) {
			return $ret;
		}

		$img_size = 'wpez_xs';

		$prev     = get_adjacent_post( false, '', true );
		$prevlink = get_permalink( get_adjacent_post( false, '', true ) );

		$next     = get_adjacent_post( false, '', false );
		$nextlink = get_permalink( get_adjacent_post( false, '', false ) );

		// Details.
		$ret .= '<details>';
		$ret .= '<summary>' . 'Blog: Next \ Prev Posts' . '</summary>';
		$ret .= '<div class="details-inner details-nav details-blog-next-prev">';
		$ret .= '<nav class="navigation post-navigation" role="navigation">';
		$ret .= '<h2 class="sr-only">Post Navigation</h2>';
		$next_prev = '';
		if ( get_permalink() !== $nextlink ) {
			$next_prev .= 'next';
		}
		if ( get_permalink() !== $prevlink ) {
			$next_prev .= 'prev';
		}
		$ret .= '<div class="blog-next-prev-wrapper ' . esc_attr( $next_prev ) .  '">';
		if ( get_permalink() !== $nextlink ) {

			$ret .= '<a href="' . esc_url( $nextlink ) . '" class="direction" title="Newer" rel="next">';
			$ret .= '<div class="nav-direction nav-next">';
			$ret .= '<div class="nav-next-label">Newer</div>';
			$ret .= '<div class="post-adjacent-title no-img">';
			$ret .= esc_html( $next->post_title );
			$ret .= '</div>';
			$ret .= '</div>';
			$ret .= '</a>';
		} else {
			$ret .= '<div class="nav-direction nav-next">';
			$ret .= '</div>';
		}

		$ret .= '<span class="nav-direction-divider">';
		$ret .= '</span>';

		if ( get_permalink() !== $prevlink ) {
			$ret .= '<a href="' .  esc_url( $prevlink ) . '" class="direction" title="Older" rel="prev">';
			$ret .=  '<div class="nav-direction nav-previous">';
			$ret .= '<div class="nav-previous-label">Older</div>';
			$ret .= '<div class="post-adjacent-title no-img">';
			$ret .= esc_html( $prev->post_title );
			$ret .= '</div>';
			$ret .= '</div>';
			$ret .= '</a>';
		} else {
			$ret .=  '<div class="nav-direction nav-previous">';
			$ret .= '</div>';
		}

		$ret .= '</div>';
		$ret .= '</nav>';
		$ret .= '</div>';
		$ret .= '</details>';

		return $this->maybeEcho( $ret, $echo );
	}

	protected function searchForm( bool $open = false, bool $active = true, bool $echo = true ){

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}
		if ( $open ) {
			$ret .= '<details open id="details-search">';
		} else {
			$ret .= '<details id="details-search">';
		}
		$ret .= '<summary>Search</summary>';
		$ret .= '<div class="details-inner details-search">';
		$ret .= get_search_form( array( 'echo' => false ) );
		$ret .= '</div>';
		$ret .= '</details>';

		return $this->maybeEcho( $ret, $echo );
	}

		protected function searchTopTerms( bool $active = true, bool $echo = true ) {

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}

		if ( has_nav_menu( 'menu_search_terms_tops' ) ) {

			$ret .= '<details>';
			$ret .= '<summary>' . 'Search: Top Terms' . '</summary>';
			$ret .= '<div class="details-inner details-nav details-search-top-terms">';
			$ret .= '<nav class="footer-nav footer-search-terms-tops">';
			$ret .= wp_nav_menu(
				array(
					'theme_location' => 'menu_search_terms_tops',
					'menu_class'     => 'menu',
					'container'      => true,
					'echo'           => false,
					'depth'          => -1,
				)
			);
			$ret .= '</nav>';
			$ret .= '</div>';
			$ret .= '</details>';
		}

		return $this->maybeEcho( $ret, $echo );
	}

	protected function sukiSocial( bool $active = true, bool $echo = true ){

		$ret = '';
		if ( true !== $active ) {
			return $ret;
		}
		$ret .= '<details id="details-au-social">';
		$ret .= '<summary>AU on Social Media</summary>';
		$ret .= '<div class="details-inner details-nav details-social">';
		ob_start();
		//suki_get_template_part( 'header-element', 'social' );
		suki_header_element( 'social' );
		$ret .= ob_get_clean();
		$ret .= '</div>';
		$ret .= '</details>';

		return $this->maybeEcho( $ret, $echo );
	}

	protected function maybeEcho( string $str = '', bool $echo = true ){

		if ( true === $echo ) {
			echo $str;
		}
		return $str;
	}


	/**
	 * TODO - ok - active status currently false
	 *
	 * @return void
	 */
	public function sukiSinglePostNavigation() {

		if ( ! is_single() ) {
			return '';
		}
?>
<details>
<summary>Next / Previous Articles</summary>
<div class="details-inner details-nav details-social">
<nav class="navigation post-navigation" role="navigation">
	<h2 class="sr-only">Post Navigation</h2>
	<div class="nav-links">
		<?php
		// $img_size = 'wpez_xs';
		$prev     = get_adjacent_post( false, '', true );
		$prevlink = get_permalink( get_adjacent_post( false, '', true ) );

		$next     = get_adjacent_post( false, '', false );
		$nextlink = get_permalink( get_adjacent_post( false, '', false ) );

		echo '<div class="nav-direction nav-next">';
		if ( $nextlink != get_permalink() ) {
			?>

			<a href="<?php echo esc_url( $nextlink ); ?>" title="Newer" rel="next">

				<?php
				/*
				$thumb_id = get_post_thumbnail_id( $next->ID );
				if ( ! empty( $thumb_id ) ) {
					$arr = wp_get_attachment_image_src( $thumb_id, $img_size );
				}
				if ( isset( $arr[0] ) ) {
					echo '<div class="post-adjacent-img-wrap">';
					echo '<img src="' . esc_url( $arr[0] ) . '" alt="' . esc_html( $next->post_title ) . '">';
					echo '<div class="post-adjacent-title">';
					echo esc_html( $next->post_title );
					echo '</div>';
					echo '<div class="nav-next-label">Newer</div>';
					echo '</div>';
				} else {
				*/
				echo '<div class="nav-next-label">Newer</div>';
				echo '<div class="post-adjacent-title no-img">';
				echo esc_html( $next->post_title );
				echo '</div>';
				// }
				?>
			</a>

		<?php }
		echo '</div>';

		echo '<div class="nav-direction nav-previous">';
		if ( $prevlink != get_permalink() ) {
		?>

		<a href="<?php echo esc_url( $prevlink ); ?>" title="Older" rel="prev">
			<?php
			/*
			$thumb_id = get_post_thumbnail_id( $prev->ID );
			if ( ! empty( $thumb_id ) ) {
				$arr = wp_get_attachment_image_src( $thumb_id, $img_size);
			}
			if ( isset( $arr[0] ) ) {
				echo '<div class="post-adjacent-img-wrap">';
				echo '<img src="' . esc_url( $arr[0] ) . '" alt="' . esc_html( $prev->post_title ) . '">';
				echo '<div class="post-adjacent-title">';
				echo esc_html( $prev->post_title );
				echo '</div>';
				echo '<div class="nav-previous-label">Older</div>';
				echo '</div>';
			}  else {
			*/
		//	$x = get_template_part( 'template-parts/', 'test' );
		//	var_dump( $x );
		//	$y = locate_template( 'template-parts/test.php' , true, false, array() );
		//	var_dump( $y );
		//	echo '<br>123<br>';
			echo '<div class="nav-previous-label">Older</div>';
			echo '<div class="post-adjacent-title no-img">';
			echo esc_html( $prev->post_title );
			echo '</div>';
			// }
			?>

		</a>

	<?php
	}
echo '</div>';
		?>

	</div>
</nav>
</div>
</details>
		<?php
	}
}
