<?php

namespace WPezThemeCustomizeSukiForAU\App\Theme\Filters;

class ClassFilters {

	public function __construct(){}


	/**
	 * TODO - ok - active currently false
	 *
	 * @param array $arr_current
	 * @return void
	 */
	public function bodyClass( $arr_current ) {

		$arr_add = [];
		if ( is_404() ) {
		//    $arr_add[] = 'has-scrolled';
		//    $arr_add[] = 'has-scrolled40';
		}
		return array_merge( $arr_current, $arr_add );
	}

	/**
	 * TODO - ok
	 *
	 * @param [type] $html
	 * @return void
	 */
	public function remove_title_404( $html ){

		if ( is_404() ) {
			return '';
		}
		return $html;
	}

	/**
	 * TODO - ok
	 *
	 * @param [type] $arr
	 * @return void
	 */
	public function sukiFrontendContentClasses( $arr ) {

		// force the container class, that defines the content width
		$arr['container'] = 'suki-section-default';

		return $arr;
	}

	/**
	 * TODO - ok
	 *
	 * @return void
	 */
	public function removeSukiGetSearchFormAddIcon() {

		remove_filter( 'get_search_form', 'suki_get_search_form_add_icon' );
	}

	/**
	 * TODO - ok
	 *
	 * @param boolean $form
	 * @param array $args
	 * @return void
	 */
	public function getSearchForm( $form = false, $args = array() ) {

		if ( is_string( $form ) ) {

			$form = preg_replace( '/placeholder="(.*?)"/', 'placeholder="' . esc_attr__( '' ) . '"', $form );
		}

		return $form;
	}



	/**
	 * TODO - ok
	 *
	 * @param [type] $arr_social
	 * @return void
	 */
	public function sukiDatasetSocialMediaTypes( $arr_social ) {

		unset( $arr_social['google-plus'] );
		unset( $arr_social['vk'] );
		$arr_gl = array( 'gitlab' => 'GitLab' );
		return array_merge( $arr_social, $arr_gl );
	}

	/**
	 * TODO - ok
	 *
	 * @param [type] $str_svg
	 * @param [type] $str_key
	 * @return void
	 */
	public function sukiFrontendSvgIcon( $str_svg, $str_key ) {

		if ( 'gitlab' === $str_key ) {
			// min'ed didn't reduce enough. left it as is. 
			return '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1"><path d="M8.383 1.973l-4.305 11.48-.242.652-2.04 5.438L16 29.875l14.203-10.332-2.039-5.438-4.55-12.132-3.731 11.48h-7.766zM8.25 8.027l1.766 5.426H6.215zm15.5 0l2.035 5.426h-3.8zM5.465 15.453h5.2l3.429 10.563-9.89-7.196zm7.3 0h6.47L16 25.403zm8.57 0h5.196l1.266 3.367-9.895 7.196z" id="surface1"/><metadata><rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:dc="http://purl.org/dc/elements/1.1/"><rdf:Description about="https://iconscout.com/legal#licenses" dc:title="gitlab" dc:description="gitlab" dc:publisher="Iconscout" dc:date="2017-12-30" dc:format="image/svg+xml" dc:language="en"><dc:creator><rdf:Bag><rdf:li>Icons8</rdf:li></rdf:Bag></dc:creator></rdf:Description></rdf:RDF></metadata></svg>';
		}

		return $str_svg;
	}


	/**
	 * TODO - ok - active currently false
	 *
	 * @param array $arr
	 * @return void
	 */
	public function frontendEntryThumbnailClasses( $arr = array() ) {

		if ( is_array( $arr ) && is_single() ) {
			return array_merge( array( 'single-hero-cover' ), $arr );
		}
		return $arr;
	}


	public function frontendCommentFormArgs( $args ) {

		$arr = array(
			'title_reply'        => 'Question? Comment?',
			'title_reply_before' => '<h4 id="reply-title" class="au-comment-reply-title">',
			'title_reply_after'  => '</h4>',
		);

		return $arr;
	}



	/**
	 * TODO - ok - active currently false
	 *
	 * @param [type] $sizes
	 * @param [type] $size
	 * @param [type] $image_src
	 * @param [type] $image_meta
	 * @param [type] $attachment_id
	 * @return void
	 */
	public function wp_calculate_image_sizes( $sizes, $size, $image_src, $image_meta, $attachment_id ) {

		// this (default) is foolish :(
		// $sizes = '(max-width: 576px) 100vw, (max-width: 768px) 100vw, (max-width: 992px) 100vw, (max-width: 1200px) 100vw, (max-width: 1500px) 100vw, 100vw';

		// grid.
		if ( is_archive() || is_home() ) {

			// full...two across...three across...
			$sizes = '(max-width: 768px) 100vw, (max-width: 1120px) 50vw, 33vw';
		} elseif ( is_search() ) {

			$sizes = '(max-width: 768px) 100vw, (max-width: 1024px) 992px, 992px';
		}

		return $sizes;
	}

}
