<?php
/**
 * Created by PhpStorm.
 */

// No namespace. Just any parent theme function you want / need to plug

if ( ! function_exists( 'suki_title__search' ) ) :
	/**
	 * Print / return HTML markup for title text for search page.
	 *
	 * @param boolean $echo
	 */
	function suki_title__search( $echo = true ) {
		$html = sprintf(
			/* translators: %s: search query. */
			esc_html__( 'Search: %s', 'suki' ),
			'' //'<span>' . get_search_query() . '</span>'
		); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

		if ( $echo ) {
			echo $html; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		} else {
			return $html;
		}
	}
	endif;


/**
 * Print entry header meta.
 */
function suki_entry_header_meta() {
	suki_entry_meta( suki_get_theme_mod( 'entry_header_meta' ) );
}

if ( ! function_exists( 'suki_entry_meta' ) ) {
	/**
	 * Print entry meta.
	 *
	 * @param string $format
	 */
	function suki_entry_meta( $format ) {
		if ( 'post' !== get_post_type() ) {
			return;
		}

		$format = trim( $format );
		$html = $format;

		if ( ! empty( $format ) ) {
			preg_match_all( '/{{(.*?)}}/', $format, $matches, PREG_SET_ORDER );

			foreach ( $matches as $match ) {
				ob_start();

				suki_entry_meta_element( $match[1] );
				$meta = ob_get_clean();

				$html = str_replace( $match[0], $meta, $html );
			}

			if ( '' !== trim( $html ) ) {
				echo '<div class="entry-meta au-entry-meta">' . $html . '</div>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}
	}
}

/**
 * ====================================================
 * Entry template functions
 * ====================================================
 */

if ( ! function_exists( 'suki_entry_meta_element' ) ) {
	/**
	 * Print entry meta element.
	 */
	function suki_entry_meta_element( $element ) {

		switch ( $element ) {
			case 'date':
				$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
				if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
					$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated screen-reader-text" datetime="%3$s">%4$s</time>';
				}
				$time_string = sprintf(
					$time_string,
					esc_attr( get_the_date( 'c' ) ),
					esc_html( get_the_date() ),
					esc_attr( get_the_modified_date( 'c' ) ),
					esc_html( get_the_modified_date() )
				);

				// echo '<span class="entry-meta-date"><a href="' . esc_url( get_permalink() ) . '" class="posted-on">' . $time_string . '</a></span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				echo '<span class="entry-meta-date">' . $time_string . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

				break;

			case 'author':
				// echo '<span class="entry-meta-author author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author_meta( 'display_name' ) ) . '</a></span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				echo '<span class="entry-meta-author author vcard">' . esc_html( get_the_author_meta( 'display_name' ) ) . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				break;

			case 'avatar':
				echo '<span class="entry-meta-author-avatar">' . get_avatar( get_the_author_meta( 'ID' ), apply_filters( 'suki/frontend/meta_avatar_size', 24 ) ) . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				break;

			case 'categories':
				$cats_temp = get_the_category_list( esc_html_x( ', ', 'terms list separator', 'suki' ) );
				if ( ! empty( $cats_temp ) ){
					echo '<span class="entry-meta-categories cat-links">' . $cats_temp . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				}
				break;

			case 'tags':
				$tags_temp = get_the_tag_list( '', esc_html_x( ', ', 'terms list separator', 'suki' ) );
				if ( ! empty( $tags_temp ) ) {
					echo ( '<span class="entry-meta-tags tags-links">' . $tags_temp . '</span>' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				}
				break;

			case 'comments':
				if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
					echo '<span class="entry-meta-comments comments-link">';
					comments_popup_link();
					echo '</span>';
				}
				break;
		}
	}
}
